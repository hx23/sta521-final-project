### Job Recommendation ###
Data Location: 
  
+  [Kaggle](https://www.kaggle.com/c/job-recommendation/data)

R Code Files:

+  read_data.R: reads data from original .tsv files into R environment. 
+  user_history_similarity.R: implements the model that finds similar users from user work history. 
+  user_user_similarity.R: implements the user-user similarity based method. 
+  job_based_recommendations.R: implements the job-job similarity based method.
+  collaborative_filtering_rating_matrix.R: converts the application data to rating matrices. 
+  collaborative_filtering_rating.R: mainly uses the recommenderlab package to create recommender models and model evaluation. 




Reading list: 
 
+  A good read about Collaborative Filtering : [pdf](http://files.grouplens.org/papers/FnT%20CF%20Recsys%20Survey.pdf)  
+  More about Collaborative Filtering : [pdf](http://delivery.acm.org/10.1145/380000/372071/p285-sarwar.pdf?ip=152.3.43.230&id=372071&acc=ACTIVE%20SERVICE&key=7777116298C9657D.18C4EEC63BFE39A6.4D4702B0C3E38B35.4D4702B0C3E38B35&CFID=558569373&CFTOKEN=23482440&__acm__=1446676878_f87cc3d708192c00239dba23d219ce2c)  
+  The recommenderlab R package : [link](https://cran.r-project.org/web/packages/recommenderlab/index.html)  
+  Handling big data in R : [link](http://davetang.org/muse/2013/09/03/handling-big-data-in-r/)   
+  For text data, topicmodels? : [link here](https://cran.r-project.org/web/packages/topicmodels/)  

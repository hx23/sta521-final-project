\documentclass{article} % For LaTeX2e

\usepackage{multicol}
\usepackage{nips15submit_e,times}
\usepackage{graphicx}
\usepackage{hyperref}
\usepackage{url}
\usepackage{subcaption}
%\documentstyle[nips14submit_09,times,art10]{article} % For LaTeX 2.09


\title{STA 521 Final Project: Job Recommendation Engine}



% \author{
% Feilun Wu \\
% \texttt{feilun.wu@duke.edu} \\
% \And
% Keye Su \\
% \texttt{keye.su@duke.edu} \\
% \AND
% Yang Su \\
% \texttt{yang.su1@duke.edu} \\
% \And
% Hong Xu \\
% \texttt{hx23@duke.edu} \\
% Duke University \\
% Durham, NC 27708
%}


% The \author macro works with any number of authors. There are two commands
% used to separate the names and addresses of multiple authors: \And and \AND.
%
% Using \And between authors leaves it to \LaTeX{} to determine where to break
% the lines. Using \AND forces a linebreak at that point. So, if \LaTeX{}
% puts 3 of 4 authors names on the first line, and the last on the second
% line, try using \AND instead of \And before the third author name.

\newcommand{\fix}{\marginpar{FIX}}
\newcommand{\new}{\marginpar{NEW}}

%\nipsfinalcopy % Uncomment for camera-ready version

\begin{document}

\maketitle

%\centerline{Authors: Hong Xu, Keye Su, Yang Su, Feilun Wu}


\begin{abstract}
Recommender systems are widely used in job search websites. In this paper, we analyzed data from CareerBuilder.com and developed different recommender algorithms to recommend jobs to users. The data provided by CareerBuilder.com contain information about users, job applications, and job descriptions. Based on the data, we implemented three recommender systems with increasing complexity: (1) a user-content based system, (2) a job-content based system, and (3) an application history based collaborative filtering system. We then evaluated the performance of each recommender system.
\end{abstract}
%\begin{multicols}{2}
\section{Introduction}
\subsection{Background}
CareerBuilder.com is one of the most visited employment websites in the United States. Reference[1], which is sponsored by CareerBuilder.com, asks us to predict what jobs users will apply for based on their previous applications, demographic information, and work history. The insights discovered in this paper will allow CareerBuilder.com to improve its job recommendation algorithm, which is a core part of the website and a key element in improving user experience.  
\subsection{Data}

\textbf{Users} contains user profile. Column \textit{UserID} assigns a unique number to each user. Column \textit{Split} tells whether a user is in training or test group. The remaining columns contain demographic and professional information about users. 

\textbf{User History} records user employment history. Each row describes a job that a user held. Column \textit{JobTitle} contains the job title and column \textit{Sequence} indicates the order in which this user held that job, with smaller numbers indicating more recent jobs.

\textbf{Jobs} contains information about job postings. Column \textit{JobID} uniquely identifies a job. Each row describes a job post (i.e. text data). Column \textit{StartDate} and column \textit{EndDate} indicate the starting and ending time for which this job posting was visible on CareerBuilder.com.

\textbf{Applications} includes application history for all users where each row describes a job application. Column \textit{UserID}, \textit{Split}, and \textit{JobID} have the same meanings as above. Column \textit{ApplicationDate} indicates the date and time at which this user applied for the job.

\subsection{Motivating Questions}  
The questions we address in this paper include: What would be the similar jobs to those a user has already applied for? Whether demographics of a user can predict the jobs s/he will apply?
Based on the similarity analysis, can we obtain a list of jobs that we can recommend to a specific user, with a satisfactory level of accuracy?
\section{Methodology}
\subsection{User-User Similarity Metric}
This metric is based on the similarities among users (i.e. job seekers). It can be inferred from dataset \textit{User History} and/or \textit{Users}.

This algorithm first investigates \textit{User History}, which contains the \textit{Job Titles} a user has held. The key idea is to find similar job titles in \textit{User History} for a new user given her/his job title(s), as shown in Table~\ref{tab:userhistory}. 

The limitation of this algorithm is that it cannot always find similar users for a new user who had a job title that cannot be found or is extremely rare in training data. Therefore, recommendation merely based on this model is \textit{not adequate}. It could be used as a prior/post processing tool which will improve the overall prediction in collaboration with other models.

Then we investigate \textit{Users}, which contains user background infomation, including major, degree, employee status and total years of experience. We transformed them into numerical features so that we can calculate distances among users. Detailed steps of processing \textit{Users} data are shown in Appendix's subsection~\ref{sssec:dp_user}.

\textbf{Algorithm}  First, for a particular user from the test data, calculate its \textit{Euclidean distances} from the users in training set. Second, find top $N$ training users who have the smallest distances from this test user, and the corresponding jobs they have applied for from \textit{Applications} data. These jobs are the recommended jobs for this test user. ($N$ is a tunable parameter.)
\subsection{Information Retrieval and Job-Job Similarity}
\textit{Jobs} dataset contains information about  \textit{Job Title}, \textit{Job Description}, and \textit{Job Requirement} for each job posted on the website. This algorithm  transforms this raw, free-form text data into an organized Document-Term Matrix, so that we can calculate the similarities among jobs. The detailed data preparation steps are included in Appendix subsection~\ref{sssec:dp_job}.

\textbf{Algorithm} First, calculate the \textit{Euclidean distances} between all job pairs based on the Document-Term Matrix. Second, form a ``similarity matrix'' for all jobs, where each row represents the closest 50 jobs for a particular job. Then, split \textit{Applications} dataset into ``prediction'' and ``evaluation'' set for each user. For the jobs in the ``prediction'' set, find top $N$ closest jobs based on ``similarity matrix'' in the previous step ($N$ is a tunable parameter), and use them as job recommendations for each user. Then the recommendations made are evaluated against the true data in ``evaluation'' set. 
\subsection{Collaborative Filtering}

\textbf{Algorithm} Collaborative filtering (CF) is a popular technique used in recommender systems. It uses given data by many users for many items (in this case, jobs) as the basis for creating a top-N recommendation list for a given user. Collaborative filtering algorithms are typically divided into two groups[3]:

\textbf{User-based Collaborative Filtering}
The assumption is that users with similar preferences will rate jobs similarly. Thus missing ratings for a user can be predicted by first finding a neighborhood of similar users and then aggregating the ratings of these users. The model tries to mimic word-of-mouth by analyzing rating data from similar individuals.

\textbf{Item-based Collaborative Filtering}
The assumption behind this approach is that users will prefer jobs that are similar to other jobs they like. The algorithm consists of constructing a similarity matrix which contains all job-to-job similarities using a given similarity measure. To make a recommendation based on the model, we use the similarities to calculate a weighted sum of the user’s ratings for related jobs.
\subsubsection*{User and Item-Based CF using 0-1 Data}
In both groups, similarities are based solely on historical applications data, which is binary data where 1 means that the user has applied for a job and 0 means that the user does not apply. We propose a similarity measure -- \textit{Jaccard index}, which only focuses on matching ones :
\begin{equation}
{sim}_{Jaccard}(X, Y) = \frac{|X\cap Y|}{|X\cup Y|}
\end{equation}
where $X$ and $Y$ are the sets of the jobs with a 1 in user profiles $u_a$ and $u_b$, respectively. The Jaccard index can be used between users for user-based filtering and between items for item-based filtering as described above. 
Data preparation is shown in Appendix's subsection~\ref{sssec:dp_cf}. 
\section{Results}
\subsection{Model Evaluation}
Across all methods, we used \textit{recall}, a.k.a \textit{true positive rate (TPR)} and \textit{precision} as the measurements of the model performance, which are defined as:
\begin{equation}
TPR = \frac{\sum_k n_{true\ positive}^k/|u_k|}{|U|}
\end{equation}
\begin{equation}
precision = \frac{\sum_k n_{true\ positive}^k/|m_k|}{|U|}
\end{equation}
where $|U|$ denotes the total number of users, $|u_k|$ denotes the total number of jobs that user $k$ has applied, $|m_k|$ denotes the total number of jobs the model recommended to user $k$, and $n_{true\ positive}^k$ is the number of jobs that are correctly recommended to user $k$ by the model.

Additionally, we compared the $TPR$ to that of a naive recommender that gives recommendations randomly. This is to establish a baseline and show how much our models have improved from it.
\subsection{Model Performance}
\subsubsection{User-User Similarity}
We recommend jobs for 1000 test users based on user-user similarity metric. The true positive rate (TPR), which is 2.19\%, is larger than that of random recommendation (0.53\%). As we increased the number of similar users used for recommendation from 10 to 50, the TPR  increased to 8.06\%, while the TPR of random recommendation remained almost the same. The plot of the number of users against TPR is shown in Figure~\ref{fig:user_user}.  
\subsubsection{Job-Job Similarity}
Figure~\ref{fig:jobs} compares the results of job-job similarity model and baseline (random) model using TPR. As can be seen, the prediction performance is significantly improved from baseline when job-job similarity model is applied. And not surprisingly, larger number of recommendations leads to a higher TPR (from 3.9\% to 7.7\% as we go from top 10 to top 50).
\subsubsection{Collaborative Filtering}
%Figure~\ref{fig:tpr_all}
The performances of a random prediction model and a prediction model based on popular jobs are shown as baselines. Clearly, the collaborative filtering (CF) models significantly outperforms the two baseline models (Figure~\ref{fig:tpr_all}). Our results show that Item-based CF (IBCF) has a higher TPR than that of User-based CF (UBCF). This indicates that \textit{Item Similarity} is more meaningful in practice as jobs are simpler, while users have various preferences and are harder to be put into similar categories. We also included the performance of job-job similarity model, which is based on job descriptions, in the plot for comparison. And Item-based CF outperforms it. This indicates that application history of a job is a more reliable source than the pure description of it. Note that the TPR saturated for both UBCF and IBCF. This is possibly due to the facts that some ``true jobs'' do not have applicants in the training dataset.

The precisions against TPR of the models are shown in Figure~\ref{fig:precrec_all}. Clearly, the random model has the lowest TPR and precision. All CF methods beat job-job similarity model in terms of TPR. All methods increase in TPR and decrease in precision as we increased the number of recommendations from 10 to 50. This makes sense because making more recommendations mean the model has a higher chance to pick out the ``true'' ones; but at the same time, giving too many recommendations will introduce more false positive thus hurt the precision. Item-based CF models have much higher precision and recall values compared with the rest of models, which is consistent with our previous explanation of its superiority. 

\begin{figure*}[hb]
  \centering
  \begin{subfigure}[t]{1\textwidth}
    \centering
    \includegraphics[height=0.3\textheight, width=0.8\linewidth]{tpr_all.png}
    \caption{Comparison of TPR between CF methods and Job-Job}
    \label{fig:tpr_all}
  \end{subfigure} %
  ~
  \begin{subfigure}[t]{1\textwidth}
    \centering
    \includegraphics[height=0.3\textheight, width=0.8\linewidth]{prerec_all.png}
    \caption{Comparison of Precision-TPR between CF methods and Job-Job}
    \label{fig:precrec_all}
  \end{subfigure}
  \caption{CF and Job-Job model Comparision}
\end{figure*}

\subsection*{Conclusion and Improvement}
Content based methods, i.e. user-user and job-job similarity models significantly outperformed the baseline model. Among all the models we investigated, Item-based Collaborative Filtering has the strongest predictive power. To further improve the performance, ensemble methods can be used to extend the current models by using the majority vote of all recommendations.
In future studies, more complex evaluation methods such as rank score, Liftindex and Discounted Cumulative Gain(DCG) can be used to further identify the strengths and weaknesses of different recommender systems.
\section*{Appendix}
\subsection*{Data Preparation}
\subsubsection*{Data Preparation in User-User Similarity Method} \label{sssec:dp_user}

a. Remove rows with NAs.

b. Select all features except geographic ones (we assume that geographic features have little influence on job application).

c. Transform feature \textit{CurrentlyEmployed} into numerical feature: if \textit{CurrentlyEmployed} = Yes, then transform it to 1, otherwise transform it to 0.

d. For categorical variables such as \textit{DegreeType} and \textit{Major}, create dummy variables for them.

\subsubsection*{Data Preparation in Job-Job Similarity Method} \label{sssec:dp_job}
Using the \textbf{tm} package in R, we utilized methods commonly used in the \textit{Information Retrievel} field: first transform text to lower case, then remove punctuation, then remove numbers, remove stopwords, and finally stem the document. After this, we scaled the text data using TF-IDF weighting.

\textbf{TF}, i.e. term frequency, ${tf}_{i,j}$ counts the number of occurrences $n_{i,j}$ of a term $t_i$ in a document $d_j$. In the case of normalization, the term frequency ${tf}_{i,j}$ is divided by $\sum_k n_{k,j}$.

\textbf{IDF}, i.e. inverse document frequency for a term $t_i$ is defined as ${idf}_i = \log \frac{|D|}{|{d \mid t_i \in d}|}$ where $|D|$ denotes the total number of documents and where $|{d \mid t_i \in d}|$ is the number of documents where the term $t_i$ appears.


What adds to complexity of the text processing task is that the Document-Term Matrix (DTM) is very sparse - in the test data, we have 1271 documents and 12145 terms, and the non null entries is only at $152513/15283782 = 0.99\%$. The high dimension of terms makes the ensuing job of calculating the document similarity computationally expensive. So we used \textbf{removeSparseTerms} function from \textbf{tm} packages to remove the terms that are at least 90\% empty. And this effectively shrinks the size of the DTM to only 287 terms.

\subsubsection*{Data Preparation in Collaborative Filtering} \label{sssec:dp_cf}
Use the \textit{Applications} data. Each application has two main features: the \textit{userID} and the \textit{jobID}. These two features are used to populate a \textit{Sparse Matrix} that users across rows and jobs across columns. This matrix is then used in the \textbf{recommenderlab}[2] package to 1) create a recommender model 2) create an evaluation scheme and split the data into training and test sets 3) evaluate the performance of the recommendation. In our specific application, the rating matrix that stores application history is a \textit{binary matrix}.

Each application has two main features: the userID and the jobID. These two features are used to populate a sparse matrix that users across rows and jobs across columns. The rating matrix is extremely sparse, which only 0.00011855 of the element is nonzero.

Due to the large size of the application matrix (requires 4.2 gb of RAM when converted to Matrix class), we randomly sampled 2000 jobs and created recommender models and evaluated the accuracy of prediction. Note that after sampling 2000 jobs (columns), the users that did not apply for any of these jobs are removed, further decreasing the size of the rating matrix and increasing the speed of the algorithm. Figure~\ref{fig:hist} shows the histograms of applicants per job and application per user before and after sampling.
\newpage
\begin{table}[ht]
\centering
\resizebox{\columnwidth}{!}{%
\begin{tabular}{rlll}
  \hline
 & Example 1 & Example 2 & Example 3 \\
  \hline
new user & Administration Receptionist & Customer Care Analyst & IT Analyst - Help Desk \\
  similar user 1 & Receptionist & Customer Care Analyst & Help Desk Analyst / Progressive IT \\
  similar user 2 & Receptionist & Solution Center Analyst -Contract for Health Care Provider & Help Desk Analyst \\
  similar user 3 & Receptionist & In-Home Care Provider-Home Instead Senior Care & Help Desk Analyst \\
  similar user 4 & Receptionist & Sr. Business Operations Analyst / Financial Analyst & Help Desk Analyst \\
  similar user 5 & Receptionist & Sales Operations Analyst, Marketing Analyst & Help Desk Analyst \\
  similar user 6 & Receptionist & Quality Control Analyst, Regional Service Analyst & Help Desk Analyst \\
  similar user 7 & Receptionist & Inventory Analyst / WIP Analyst & Help Desk Analyst \\
  similar user 8 & Receptionist & Assistant Director / Ambulatory Care - Primary Care Services & IT Analyst - Help Desk \\
  similar user 9 & Receptionist & Sr. Business Analyst / Lead Analyst - Digital Information Technology & Sr. Help Desk Analyst / HD Level \\
  similar user 10 & Receptionist & Sr. Service Desk Analyst/ Sr Desktop Analyst/Jr Software Triage Analyst & Level 1 Help Desk Analyst \\
   \hline
\end{tabular}
}
\caption{Job Titles of Similar Users based on User History}
\label{tab:userhistory}
\end{table}

\begin{figure}[h]
  \centering
  \includegraphics[scale=0.5]{user_user.png}
  \caption{TPR of User Similarity Based Recommendation}
  \label{fig:user_user}
\end{figure}
\begin{figure}[h]
  \centering
  \includegraphics[scale=0.5]{eva_job.png}
  \caption{TPR of Job Similarity Based Recommendation}
  \label{fig:jobs}
\end{figure}
\newpage
\begin{figure}[h]
    \centering
    \includegraphics[scale=0.5]{histogram_3_combined.png}
    \caption{Histograms of the original application data (the first row) and the sampled application data based on users (the second row) and jobs (the third row)}
    \label{fig:hist}
\end{figure}
\newpage

\subsubsection*{References}
\small{
[1] Kaggle, Job Recommendation Challenge, https://www.kaggle.com/c/job-recommendation/data

[2] Hahsler, M. Recommenderlab: A Framework for Developing and Testing Recommendation Algorithms, 2011

[3] Ekstrand, M. D. Riedl, J. T. \& Konstan, J. A. Collaborative Filtering Recommender Systems, Human–Computer Interaction, Vol. 4, No. 2 (2010) 81–173
}

%\end{multicols}
\end{document}




#install.packages("recommenderlab")
require("recommenderlab")


# Follow the example in recommenderlab documentation--------------------------

###############################
# CHUNK I: REDUCE # of USERS
###############################
# create a function that samples certain number (numUser)
# of users in the entire rating matrix
sampleRating<-function(Rmatrix,numUser){
  set.seed(1)
  rowNums<-sample(1:dim(Rmatrix)[1],numUser)
  Rmatrix_numUser<-Rmatrix[rowNums,]
  CS<-colSums(Rmatrix_numUser)
  colNumNonZero<-which(CS!=0)
  Rmatrix_sampled<-Rmatrix_numUser[,colNumNonZero]
  return(Rmatrix_sampled)
}
# load matrix
load("m.RData")

# SAMPLE 5000 of the users--------------------------------
mSampled<-sampleRating(m,5000)
# EXPLORE the big rating matrix and the matrix after 
# sampling
par(mfrow=c(2,2))
mRS<-rowSums(m)
hist(log(mRS),breaks=20,
     xlab="log(#apps/user)",
     ylab="frequency",
     main="Original")
mCS<-colSums(m)
hist(log(mCS),breaks=15,
     xlab="log(#apps/job)",
     ylab="frequency",
     main="Original")
mRSSmapled<-rowSums(mSampled)
hist(log(mRSSmapled),breaks=15,
     xlab="log(#apps/user)",
     ylab="frequency",
     main="Sampled")
mCSSmapled<-colSums(mSampled)
hist(log(mCSSmapled),breaks=10,
     xlab="log(#apps/job)",
     ylab="frequency",
     main="Sampled")

# Make the binary rating matrix from 
# sampled data
R_mSampled<-new("realRatingMatrix", data = mSampled)
dimnames(R_mSampled)<-list(NULL,NULL)
R_mSampled_binary <- binarize(R_mSampled,minRating=0.5)

# Make the evaluation scheme
set.seed(1)
e <- evaluationScheme(R_mSampled_binary,method="split",
                      train=0.9,given=1)

# Evaluate different recommender models using
# the same scheme
# results_AR<-evaluate(e,method="AR",n=1) # error 
# results_IBCF<-evaluate(e,method="IBCF",n=1) # too slow
results_POP<-evaluate(e,method="POPULAR",n=c(10,20,30,40,50))
results_RAN<-evaluate(e,method="RANDOM",n=c(10,20,30,40,50))
results_UBCF<-evaluate(e,method="UBCF",n=c(10,20,30,40,50))

# save the results
save(results_POP=results_POP,results_RAN=results_RAN,
     results_UBCF=results_UBCF,file="evaluateResults.RData")

# get confusion matrices for different mothods
CMatrix_POP<-getConfusionMatrix(results_POP)[[1]]
CMatrix_RAN<-getConfusionMatrix(results_RAN)[[1]]
CMatrix_UBCF<-getConfusionMatrix(results_UBCF)[[1]]

# plot recall vs number of recommendation
# for different methods
plot(c(10,20,30,40,50), CMatrix_UBCF[,"recall"], 
     type = "l",
     lwd = 2,
     ylim = c(0, 0.2),
     xlab = "Number of Recommendations Made",
     ylab = "True Positive Rate",
     col = "firebrick")
points(c(10,20,30,40,50), CMatrix_UBCF[,"recall"],
       pch = 19,
       col = "firebrick")
lines(c(10,20,30,40,50), CMatrix_RAN[1:5,"recall"],
      lwd = 2,
      col = "deepskyblue")
points(c(10,20,30,40,50), CMatrix_RAN[1:5,"recall"],
       pch = 17,
       lwd = 3,
       col = "deepskyblue")
lines(c(10,20,30,40,50), CMatrix_POP[1:5,"recall"],
      lwd = 2,
      col = "gold")
points(c(10,20,30,40,50), CMatrix_POP[1:5,"recall"],
       pch = 15,
       lwd = 3,
       col = "gold")
legend("topleft", col = c("firebrick", "deepskyblue","gold"), lwd = 2,
       legend = c("UBCF", "Baseline (Random)",
                  "Popular Jobs"))

# make recommender model using different models
rec_UBCF<-Recommender(data=getData(e,"train"),method="UBCF")
rec_RAN<-Recommender(data=getData(e,"train"),method="RANDOM")
rec_POP<-Recommender(data=getData(e,"train"),method="POPULAR")
# predict recommendations based on different models
pre_UBCF<-predict(rec_UBCF,getData(e,"known"),type="ratings")
pre_RAN<-predict(rec_RAN,getData(e,"known"),type="ratings")
pre_POP<-predict(rec_POP,getData(e,"known"),type="topNList")

# calucalte the accuracies
Uerror_UBCF<-calcPredictionAccuracy(pre_UBCF,getData(e,"unknown"),
                                    given=4,byUser=TRUE)
Uerror_POP<- calcPredictionAccuracy(pre_POP, getData(e,"unknown"),
                                    given=4,byUser=TRUE)
Uerror_RAN<- calcPredictionAccuracy(pre_RAN, getData(e,"unknown"),
                                    given=4,byUser=TRUE)


# evaluate the overall parameters of UBCF and POPULAR 
UTPR_UBCF <- Uerror_UBCF[,"TPR"]
U_given <- rowSums(getData(e,"unknown"))
plot(U_given,UTPR_UBCF)

###############################
# CHUNK II: REDUCE # of JOBS
###############################
# create a function that samples certain number (numJob)
# of jobs from the entire rating matrix
sampleRatingJ<-function(Rmatrix,numJob){
  set.seed(1)
  colNums<-sample(1:dim(Rmatrix)[2],numJob)
  Rmatrix_numJob<-Rmatrix[,colNums]
  RS<-rowSums(Rmatrix_numJob)
  rowNumNonZero<-which(RS!=0)
  Rmatrix_sampled<-Rmatrix_numJob[rowNumNonZero,]
  return(Rmatrix_sampled)
}
# load matrix
load("m.RData")

# SAMPLE 2000 of the Jobs--------------------------------
mSampledJ<-sampleRatingJ(m,2000)
# EXPLORE the big rating matrix and the matrix after 
# sampling
par(mfrow=c(2,2))
mRS<-rowSums(m)
hist(log(mRS),breaks=20,
     xlab="log(#apps/user)",
     ylab="frequency",
     main="Original")
mCS<-colSums(m)
hist(log(mCS),breaks=15,
     xlab="log(#apps/job)",
     ylab="frequency",
     main="Original")
mRSSmapledJ<-rowSums(mSampledJ)
hist(log(mRSSmapledJ),breaks=10,
     xlab="log(#apps/user)",
     ylab="frequency",
     main="Sampled")
mCSSmapledJ<-colSums(mSampledJ)
hist(log(mCSSmapledJ),breaks=10,
     xlab="log(#apps/job)",
     ylab="frequency",
     main="Sampled")

# Make the binary rating matrix from 
# sampled data
R_mSampledJ<-new("realRatingMatrix", data = mSampledJ)
dimnames(R_mSampledJ)<-list(NULL,NULL)
R_mSampled_binaryJ <- binarize(R_mSampledJ,minRating=0.5)

# Make the evaluation scheme
set.seed(1)
eJ <- evaluationScheme(R_mSampled_binaryJ,method="split",
                      train=0.9,given=1)

# Evaluate different recommender models using
# the same scheme
# results_AR<-evaluate(e,method="AR",n=1) # error 
# results_IBCF<-evaluate(e,method="IBCF",n=1) # too slow
results_POPJ<-evaluate(eJ,method="POPULAR",n=c(10,20,30,40,50))
results_RANJ<-evaluate(eJ,method="RANDOM",n=c(10,20,30,40,50))
results_UBCFJ<-evaluate(eJ,method="UBCF",n=c(10,20,30,40,50))
results_IBCFJ<-evaluate(eJ,method="IBCF",n=c(10,20,30,40,50))

plot(results_POP,"prec/rec",annotate=TRUE)
plot(results_UBCF,"prec/rec",annotate=TRUE)
plot(results_RAN,"prec/rec",annotate=TRUE)

# save the results
save(results_POPJ=results_POPJ,results_RANJ=results_RANJ,
     results_UBCFJ=results_UBCFJ,
     results_IBCFJ=results_IBCFJ,
     file="evaluateResultsJ.RData")

# get confusion matrices for different mothods
CMatrix_POPJ<-getConfusionMatrix(results_POPJ)[[1]]
CMatrix_RANJ<-getConfusionMatrix(results_RANJ)[[1]]
CMatrix_UBCFJ<-getConfusionMatrix(results_UBCFJ)[[1]]
CMatrix_IBCFJ<-getConfusionMatrix(results_IBCFJ)[[1]]

# plot recall vs number of recommendation
# for different methods
par(mfrow= c(1,1))
plot(c(10,20,30,40,50), CMatrix_UBCFJ[,"recall"], 
     type = "l",
     lwd = 2,
     ylim = c(0, 1),
     xlab = "Number of Recommendations Made",
     ylab = "True Positive Rate",
     col = "firebrick")
points(c(10,20,30,40,50), CMatrix_UBCFJ[,"recall"],
       pch = 19,
       col = "firebrick")
lines(c(10,20,30,40,50), CMatrix_RANJ[1:5,"recall"],
      lwd = 2,
      col = "deepskyblue")
points(c(10,20,30,40,50), CMatrix_RANJ[1:5,"recall"],
       pch = 17,
       lwd = 3,
       col = "deepskyblue")
lines(c(10,20,30,40,50), CMatrix_POPJ[1:5,"recall"],
      lwd = 2,
      col = "gold")
points(c(10,20,30,40,50), CMatrix_POPJ[1:5,"recall"],
       pch = 15,
       lwd = 3,
       col = "gold")
lines(c(10,20,30,40,50), CMatrix_IBCFJ[1:5,"recall"],
      lwd = 2,
      col = "green")
points(c(10,20,30,40,50), CMatrix_IBCFJ[1:5,"recall"],
       pch = 15,
       lwd = 3,
       col = "green")
legend("topleft", col = c("firebrick", "deepskyblue","gold"), lwd = 2,
       legend = c("UBCF", "Baseline (Random)",
                  "Popular Jobs"))

par(mfrow=c(2,2))
plot(results_POPJ,"prec/rec",annotate=TRUE)
plot(results_IBCFJ,"prec/rec",annotate=TRUE)
plot(results_UBCFJ,"prec/rec",annotate=TRUE)
plot(results_RANJ,"prec/rec",annotate=TRUE)


## plot the histogram
par(mfrow=c(3,2))
mRS<-rowSums(m)
hist(log(mRS),breaks=20,
     xlab="log(#apps/user)",
     ylab="frequency",
     main="Original")
mCS<-colSums(m)
hist(log(mCS),breaks=15,
     xlab="log(#apps/job)",
     ylab="frequency",
     main="Original")
mRSSmapled<-rowSums(mSampled)
hist(log(mRSSmapled),breaks=10,
     xlab="log(#apps/user)",
     ylab="frequency",
     main="Sampled Users")
mCSSmapled<-colSums(mSampled)
hist(log(mCSSmapled),breaks=10,
     xlab="log(#apps/job)",
     ylab="frequency",
     main="Sampled Users")
mRSSmapledJ<-rowSums(mSampledJ)
hist(log(mRSSmapledJ),breaks=10,
     xlab="log(#apps/user)",
     ylab="frequency",
     main="Sampled Jobs")
mCSSmapledJ<-colSums(mSampledJ)
hist(log(mCSSmapledJ),breaks=10,
     xlab="log(#apps/job)",
     ylab="frequency",
     main="Sampled Jobs")
